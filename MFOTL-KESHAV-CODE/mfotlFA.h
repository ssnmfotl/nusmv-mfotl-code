#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#include<iostream>
#include<stack>
#include<string>
#include<stdio.h>
using namespace std;
#define MAX 100
 
//-------------------------------------------------------------------------------------------
//											SECOND VERSION
//-------------------------------------------------------------------------------------------
struct tree
{
    string value;
    tree* left, *right;
};
 



//-------------------------------------------------------------------------------------------
//	Structure Declarations
//-------------------------------------------------------------------------------------------
typedef struct bal_stack
{
	char formula[MAX];
	int top;
}bal_stack;
//-------------------------------------------------------------------------------------------
typedef struct FTree
{
//	int leaf;
	int type;//type can either be 0 (leaf or proposition) or 1 (unary opearator) or 2(binary operator) or 3 (quantifier) or 4 (predicate)
	int subd;//0 for not substituted 1 for sustituted---useful only when type is 0
//	int global;//global==0,local=={1,2,...,n}
	unsigned int model;//model as a positive integer; can be converted to boolean string whenever needed
	unsigned int copy;//copy of the model above
	unsigned int num;//num=R*model+copy
	char val[10];//
	struct FTree* parent;
	struct FTree* left;
	struct FTree* right;
	struct FTree* next;
}FTree;
//--------------------------------------------------------------------------------------------
typedef struct FInfo{
	unsigned int M;//the no of models for this formula M=2^PNum
	unsigned int R;//the no. of copies of models for this formula R=VNum
	int PNum;
	int VNum;
	struct List* PList;
	struct List* VList;
}FInfo;

//-------------------------------------------------------------------------------------------
typedef struct List{
	int size;
	struct List_node* first;
	struct List_node* last;
}List;

//-------------------------------------------------------------------------------------------
typedef struct List_node{
	char val[10];
	struct List_node* left;
	struct List_node* right;
}List_node;
//--------------------------------------------------------------------------------------------
typedef struct atom_set{
	int size;
	struct atom_node* first;
	struct atom_node* last;
}atom_set;
//--------------------------------------------------------------------------------------------
typedef struct atom_node{
	int atom_val;
	int init;
	struct atom_node* left;
	struct atom_node* right;
}atom_node;
//--------------------------------------------------------------------------------------------
/*
typedef struct atom_record{
	int atom_val;
	int neg_val;
}atom_record;
*/
//--------------------------------------------------------------------------------------------
typedef struct state_record{
	int atom_val;
	int untl_val;
	int init;
}state_record;
//-------------------------------------------------------------------------------------------
typedef struct state_node{
	struct state_record* sr;
	struct state_node* left;
	struct state_node* right;
}state_node;
//--------------------------------------------------------------------------------------------
typedef struct state_set{
	int size;
	struct state_node* first;
	struct state_node* last;
}state_set;
//--------------------------------------------------------------------------------------------
typedef struct transn_record{
	state_record* sr1;
	state_record* sr2;
}transn_record;
//--------------------------------------------------------------------------------------------
typedef struct transn_node{
	struct transn_record* tr;
	struct transn_node* left;
	struct transn_node* right;
}transn_node;
//--------------------------------------------------------------------------------------------
typedef struct transn_set{
	int size;
	struct transn_node* first;
	struct transn_node* last;
}transn_set;
//-------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------
extern char bool_atom[MAX];//atom info in boolean form--returned by bool_conversion_atom routine
extern char bool_untl[MAX];//until requiremnet set in boolean form--returned by bool_conversion_untl routine
//-------------------------------------------------------------------------------------------------------
extern int i_subf;
extern int i_neg_subf;
extern int i_untilf;
extern int i_nextf;
//-------------------------------------------------------------------------------------------------------
extern FTree* subf[100];
extern FTree* neg_subf[100];
extern FTree* untilf[50];
extern FTree* nextf[50];
//----------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
extern int prcd(char);
extern int isoperator(char);
//---------------------------------------------------------------------------------------
extern FTree* for_stack[MAX];
extern FTree* op_stack[MAX];
//------------------------------------------------------------------------------------------
extern int for_top;
extern int op_top;
//-------------------------------------------------------------------------------------------
extern bal_stack* st;
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//	Function Declarations
//------------------------------------------------------------------------------------------
void substitute(FTree*,char[],int,int,int);
//------------------------------------------------------------------------------------------
void model_construct(FTree*,FInfo*);
//------------------------------------------------------------------------------------------
FTree* eliminate_quantifier(FTree*,FInfo*);
//------------------------------------------------------------------------------------------	
void add_to_state_set(state_set*,state_node*);
//------------------------------------------------------------------------------------------
void add_to_transn_set(transn_set*,state_node*,state_node*);
//------------------------------------------------------------------------------------------
transn_set* transn_construct(state_set*);
//------------------------------------------------------------------------------------------
state_set* state_construct(atom_set*);
//------------------------------------------------------------------------------------------
atom_set* atom_construct(FTree*);
//--------------------------------------------------------------------------------------------
void subf_construct(FTree*);
//--------------------------------------------------------------------------------------------
int member_untl(FTree*,char[]);
//------------------------------------------------------------------------------------------
int member_atom(FTree*,char[]);
//------------------------------------------------------------------------------------------
int find_in_closure_set(FTree*);
//------------------------------------------------------------------------------------------
int find_in_untilf_set(FTree*);
//------------------------------------------------------------------------------------------
state_node* find_state(state_set*,state_record*);
//------------------------------------------------------------------------------------------
int compare(FTree*, FTree*);
//------------------------------------------------------------------------------------------
int ll0_pow(int,int);
//------------------------------------------------------------------------------------------
FTree* copy(FTree*);
//--------------------------------------------------------------------------------------------
FTree* negate(FTree*);
//--------------------------------------------------------------------------------------------
FTree* add_next(FTree*);
//--------------------------------------------------------------------------------------------
int mod_type(char);
//------------------------------------------------------------------------------------------
char* bool_conversion_atom(int n);
//-----------------------------------------------------------------------------------------------
char* bool_conversion_untl(int n);
//-----------------------------------------------------------------------------------------------
//the following function converts a formula given as a string to a formula tree where each node is of the type struct FTree and returns the root pointer.
//input to the formula-to-tree conversion routine is given as a* *fully parenthesised** formula.
FTree* FTree_convert(char[],FInfo*);
//---------------------------------------------------------------------------------------------------
int add_predicate_to_formula_info(char[],FInfo*);
//---------------------------------------------------------------------------------------------------
int add_variable_to_formula_info(char[],FInfo*);
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------
int strchk(char*);
//--------------------------------------------------------------------------------------------
//the following function displays the tree with the root pointer given as input, basically does the inorder traversal of the tree.
void display_FTree(FTree*);
void display_FTree_pre(FTree*);
extern void display_FTree_sub(FTree* TFormula, bool flag=true);
void display_FTree_pre_sub(FTree*);
//--------------------------------------------------------------------------------------------
FTree* for_pop();
void for_push(FTree*);
//---------------------------------------------------------------------------------------------
FTree* op_pop();
void op_push(FTree*);
//-------------------------------------------------------------------------------------------
char bal_pop();
void bal_push(char c);
int isempty();
int isfull();
int check_paranthesis(char*);
//---------------------------------------------------------------------------------------------
void display_op_stack();
void display_for_stack();
