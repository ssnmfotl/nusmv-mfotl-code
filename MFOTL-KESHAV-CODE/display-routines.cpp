#include "mfotlFA.h"
#include <iostream>

using namespace std;

char c;
void display_FTree(FTree* TFormula)
{
	if(TFormula==NULL){
//		printf("\nError in the formula tree.\n")
		return;
	}
	switch(TFormula->type){

		case 4: ////printf("\nThe node type is %d",TFormula->type);
			printf("%s",TFormula->val);
			display_FTree(TFormula->next);
			break;

		case 3: ////printf("\nThe node type is %d",TFormula->type);
			printf("%s",TFormula->val);
			display_FTree(TFormula->next);
			break;

		case 2: ////printf("\nThe node type is %d",TFormula->type);
			display_FTree(TFormula->left);
			printf("%s",TFormula->val);
			display_FTree(TFormula->right);
			break;

		case 1: ////printf("\nThe node type is %d",TFormula->type);
			printf("%s",TFormula->val);
			display_FTree(TFormula->next);
			break;

		case 0: ////printf("\nThe node type is %d",TFormula->type);
			printf("%s",TFormula->val);			
			break;

		default:
			printf("\nError! Type can either be 0, 1 or 2.\n");
			break;
	}

}
//--------------------------------------------------------------------------------------------------------------------------------------------
void display_FTree_pre(FTree* TFormula)
{
	
	if(TFormula==NULL){
//		printf("\nError in the formula tree.\n")
		return;
	}
	switch(TFormula->type){

		case 4: ////printf("\nThe node type is %d",TFormula->type);
			printf("%s",TFormula->val);
			display_FTree_pre(TFormula->next);
			break;

		case 3: ////printf("\nThe node type is %d",TFormula->type);
			printf("%s",TFormula->val);
			display_FTree_pre(TFormula->next);
			break;

		case 2: ////printf("\nThe node type is %d",TFormula->type);
			printf("%s",TFormula->val);
			display_FTree_pre(TFormula->left);
			display_FTree_pre(TFormula->right);
			break;

		case 1: ////printf("\nThe node type is %d",TFormula->type);
			printf("%s",TFormula->val);
			display_FTree_pre(TFormula->next);
			break;

		case 0: ////printf("\nThe node type is %d",TFormula->type);
			printf("%s",TFormula->val);			
			break;

		default:
			printf("\nError! Type can either be 0, 1 or 2.\n");
			break;
	}

}

//--------------------------------------------------------------------------------------------------------------------------------------------
void display_FTree_sub(FTree* TFormula, bool flag)
{
	
	FILE *fptr;
    fptr = fopen("LTLSPEC.txt","a");
    
    if(fptr == NULL)
   	{
        printf("Error!");
        exit(1);
    }
    
	if(TFormula==NULL){
//		printf("\nError in the formula tree.\n")
		return;
	}
	switch(TFormula->type){

		case 4: ////printf("\nThe node type is %d",TFormula->type);
			printf("%s",TFormula->val);
			fprintf(fptr," %s ",TFormula->val);
            fclose(fptr);
            
			display_FTree_sub(TFormula->next,false);
			break;

		case 3: ////printf("\nThe node type is %d",TFormula->type);
			printf("%s",TFormula->val);

			fprintf(fptr," %s ",TFormula->val);
            fclose(fptr);

			display_FTree_sub(TFormula->next);
			break;

		case 2: ////printf("\nThe node type is %d",TFormula->type);
			printf("(");
            fprintf(fptr," ( ");
            fclose(fptr);
            
			display_FTree_sub(TFormula->left);
			//printf("%c",')');
			fptr = fopen("LTLSPEC.txt","a");
            
            if(fptr == NULL)
            {
                printf("Error!");
                exit(1);
            }
            printf("%s",TFormula->val);
            fprintf(fptr," %s ",TFormula->val);
            fclose(fptr);
			//printf("%c",'(');
			
			display_FTree_sub(TFormula->right);
			fptr = fopen("LTLSPEC.txt","a");
            
            if(fptr == NULL)
            {
                printf("Error!");
                exit(1);
            }
            printf(")");
            fprintf(fptr," ) ");
            fclose(fptr);
            
			break;

		case 1: ////printf("\nThe node type is %d",TFormula->type);
		if(strcmp(TFormula->val,"~")==0)
			strcpy(TFormula->val,"!");
			fprintf(fptr," %s ",TFormula->val);
            printf("Printing %s to file ",TFormula->val);
            fclose(fptr);
            
            display_FTree_sub(TFormula->next);
            break;
            

		case 0: ////printf("\nThe node type is %d",TFormula->type);
			if(TFormula->subd)
				{
					if(!flag){
					printf("[%d]",TFormula->num+1);
					fprintf(fptr," [%d] ",TFormula->num+1);	
                	fclose(fptr);	
                }
                	else
                	{
                		printf("%d",TFormula->num+1);
					fprintf(fptr," %d ",TFormula->num+1);	
                	fclose(fptr);
                	
                	}
				}		
			else{
                
                printf("not exploring %s",TFormula->val);
                fprintf(fptr," %s ",TFormula->val);
                fclose(fptr);
                
                
            }
			break;

		default:
			printf("\nError! Type can either be 0, 1 or 2.\n");
			break;
	}

}
//--------------------------------------------------------------------------------------------------------------------------------------------
void display_FTree_pre_sub(FTree* TFormula)
{
	
	if(TFormula==NULL){
//		printf("\nError in the formula tree.\n")
		return;
	}
	switch(TFormula->type){

		case 4: ////printf("\nThe node type is %d",TFormula->type);
			printf("%s",TFormula->val);
			display_FTree_pre_sub(TFormula->next);
			break;

		case 3: ////printf("\nThe node type is %d",TFormula->type);
			printf("%s",TFormula->val);
			display_FTree_pre_sub(TFormula->next);
			break;

		case 2: ////printf("\nThe node type is %d",TFormula->type);
			printf("%s",TFormula->val);
			display_FTree_pre_sub(TFormula->left);
			display_FTree_pre_sub(TFormula->right);
			break;

		case 1: ////printf("\nThe node type is %d",TFormula->type);
			printf("%s",TFormula->val);
			display_FTree_pre_sub(TFormula->next);
			break;

		case 0: ////printf("\nThe node type is %d",TFormula->type);
			if(TFormula->subd)
				printf("[%d]",TFormula->num);			
			else
				printf("%s",TFormula->val);
			break;

		default:
			printf("\nError! Type can either be 0, 1 or 2.\n");
			break;
	}

}

//--------------------------------------------------------------------------------------------------------------------------------------------
 
void display_for_stack()
{
	int i;
	printf("\nContents of the formula stack are...................->top of the stack->\n");
	for(i=0;i<=for_top;i++){
		printf("(%p,%s)\t",for_stack[i],for_stack[i]->val);
	}
}
//--------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------------

void display_op_stack()
{

	int i;
	printf("\nContents of the operation stack are...................->top of the stack->\n");
	for(i=0;i<=op_top;i++){
		printf("(%p,%s)\t",op_stack[i],op_stack[i]->val);
	}
}

//--------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------------

