//dev: either reduce or increase
//dev: output file junk

#include "mfotlFA.h"
#include <math.h>
#include <string.h>
//-------------------------------------------------------------------------------------------------------
char bool_atom[MAX];//atom info in boolean form--returned by bool_conversion_atom routine
char bool_untl[MAX];//until requiremnet set in boolean form--returned by bool_conversion_untl routine
//-------------------------------------------------------------------------------------------------------
int i_subf=0;
int i_neg_subf=0;
int i_untilf=0;
int i_nextf=0;
//-------------------------------------------------------------------------------------------------------
FTree* subf[100];
FTree* neg_subf[100];
FTree* untilf[50];
FTree* nextf[50];
//----------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
int prcd(char);
int isoperator(char);
//---------------------------------------------------------------------------------------
FTree* for_stack[MAX];
FTree* op_stack[MAX];
//------------------------------------------------------------------------------------------
int for_top = -1;
int op_top = -1;
//------------------------------------------------------------------------------------------



//USE ONLY ~ FOR NOT AND != FOR NOT EQUAL TO
//-------------------------------------------------------------------------------------------
//											SECOND VERSION
//-------------------------------------------------------------------------------------------



bool isOpenParen1(char c) {return (c=='(')? true: false;}
bool isCloseParen1(char c) {return (c==')')? true: false;}
bool isE1(char c)    {return (c=='E')? true : false;}
bool isA1(char c)    {return (c=='A')? true : false;}
bool isPredicate1(char c)    {return (c=='p'||c=='q'||c=='r'||c=='s'||c=='t')?   true:   false;}
bool isLTL1(char c)  {return (c=='G'||c=='F')? true: false;}
bool isIden1(char c) {return (c=='x'||c=='y'||c=='z'||c=='1'||c=='2'||c=='3'||c=='4'||c=='5'||c=='6'||c=='7'||c=='8'||c=='9'||c=='0')?   true:   false;}
bool isOperator1(char c)
{
    if (c == '=' || c == '&' || c == '|' || c == '!')
        return true;
    return false;
}
bool isNot1(string s)        {return(s[0]=='~')? true:   false;}
bool isNot1(char s)        {return(s=='~')? true:   false;}
bool isUnaryOp1(string s)   {/*cout<<"\nHere in s-un\n"<<s<<endl;*/return (s[0]=='E' || s[0]=='A' || s[0]=='G' || s[0]=='F' || s[0]=='~')? true: false;}
bool isBinaryOp1(string s)    {/*cout<<"\nHere in s-bin\n"<<s<<endl;*/return (s[0]=='=' || s[0]=='&' || s[0]=='|' || s[0]=='!')?    true:   false;}
bool isUnaryOp1(char s)   {/*cout<<"\nHere in c-un\n"<<s;*/ return (s=='E' || s=='A' || s=='G' || s=='F' || s=='~')? true: false;}
bool isBinaryOp1(char s)    {/*cout<<"\nHere in c-bin\n"<<s;*/return (s=='=' || s=='&' || s=='|' || s=='!')?    true:   false;}
 

void inorder1(tree *t)
{
    if(t)
    {
        
        inorder1(t->left);
        cout<<t->value;     //switch the position of this accordingly for pre and post
        inorder1(t->right);
    }
}


void construct_ftree(tree * &t, FTree * &ftree, FInfo * &formula_info)
{
    if(t)
    {
        ftree = new FTree;
        //ftree->left = new FTree;
        //ftree->right = new FTree;
        cout<<"\nTree node "<<t->value<<endl;
        if(isE1(t->value[0]) || isA1(t->value[0]))
        {
            //op_node->next=for_node_n;
            strcpy(ftree->val,t->value.c_str());
            ftree->type=3;//for quantifiers
            ftree->model=-1;//
            ftree->copy=-1;//
            ftree->subd=0;//
            ftree->num=-1;
            construct_ftree(t->left,ftree->left, formula_info);
            construct_ftree(t->right,ftree->next, formula_info);
        }
        else if(isUnaryOp1(t->value[0]) || isLTL1(t->value[0]))
        {
            //ftree->next=t->right;
            strcpy(ftree->val,t->value.c_str());
            ftree->type=1;//for unary operator
            ftree->model=-1;//for unary operator
            ftree->copy=-1;//for unary operator
            ftree->subd=0;//for unary operator
            ftree->num=-1;
            construct_ftree(t->left,ftree->left, formula_info);
            construct_ftree(t->right,ftree->next, formula_info);
        }
        else if(isBinaryOp1(t->value[0]))
        {
            //ftree->left=t->left;
            //ftree->right=t->right;
            strcpy(ftree->val,t->value.c_str());
            ftree->type=2;//for binary operator
            ftree->model=-1;//for unary operator
            ftree->copy=-1;//for unary operator
            ftree->subd=0;//for unary operator
            ftree->num=-1;
            construct_ftree(t->left,ftree->left, formula_info);
            construct_ftree(t->right,ftree->right, formula_info);
        }
        
        else if(isPredicate1(t->value[0]))
        {
            //op_node->next=for_node_n;
            //strcpy(ftree->val,t->value.c_str());
            char temp[MAX], temp1[MAX];
            int i;
            for(i=0;t->value[i]!='(';i++)
                temp[i]=t->value[i];
            temp[i]='\0';
            cout<<"\ntemp "<<temp;
            strcpy(ftree->val,temp);


            ftree->type=4;//for pred
            ftree->model=-1;//
            ftree->copy=-1;//
            ftree->subd=0;//
            ftree->num=-1;

            ftree->next = new FTree;
            //strcpy(ftree->val,t->value.c_str());
            ++i;
            cout<<"\nAt i "<<t->value[i]<<endl;
            int j;
            for(j=0;t->value[i]!=')';i++,j++)
                temp1[j] = t->value[i];
            temp1[j]='\0';
            cout<<"\ntemp1 "<<temp1;
            strcpy(ftree->next->val,temp1);
            ftree->next->type=0;
            ftree->next->model=-1;
            ftree->next->copy=-1;
            ftree->next->subd=0;
            ftree->next->num=-1;

            int done;
            done=add_predicate_to_formula_info(ftree->val,formula_info);
            //ftree=ftree->right;
            FTree *ftree1 = ftree->next;
            if(done)
                formula_info->PNum++;

            done=add_variable_to_formula_info(ftree->next->val,formula_info);
            if(done)
                formula_info->VNum++;

            construct_ftree(t->left,ftree1->left, formula_info);
            construct_ftree(t->right,ftree1->right, formula_info);
        }
        else
        {
            strcpy(ftree->val,t->value.c_str());
            ftree->type=0;
            ftree->model=-1;
            ftree->copy=-1;
            ftree->subd=0;
            ftree->num=-1;
            int done;
            done=add_variable_to_formula_info(ftree->val,formula_info);
            if(done)
                formula_info->VNum++;
            construct_ftree(t->left,ftree->left, formula_info);
            construct_ftree(t->right,ftree->right, formula_info);
        }
        
    }
    else
    ftree=NULL;

}

//-------------------------------------------------------------------------------------------
//											END OF SECOND VERSION
//-------------------------------------------------------------------------------------------




tree* newNode(string value)
{
    tree *temp = new tree;
    temp->left = temp->right = NULL;
    temp->value = value;
    return temp;
};

tree* newNode(char value)
{
    string val(1,value);
    tree *temp = new tree;
    temp->left = temp->right = NULL;
    temp->value = val;
    return temp;
}

 

tree* constructTree1(string formula)
{
    stack<tree *> operand,optr;
    tree *t, *t1, *t2, *t_op;
    tree *ltl;
    // Traverse through every character of
    // input expression
    for (int i=0; i<formula.size(); i++)
    {

        //cout<<"\nValue of i "<<i<<"  "<<formula[i]<<endl;

        if(formula[i]==' ') continue;

        else if(isOpenParen1(formula[i]))
        {
            string s(1,formula[i]);
            t = newNode(s);
            optr.push(t);
        }


        else if(isPredicate1(formula[i]))
        {
            string s(1,formula[i]);
            i++;
            int j;
            for(j=i;formula[j]!=')';j++)
                if(formula[j]!=' ')
                s+=formula[j];
            s+=formula[j];
            t = newNode(s);
            operand.push(t);
            i=j;
        }

        else if(isIden1(formula[i]))
        {
            int j;
            string s(1,formula[i]);
            for(j=i+1;isIden1(formula[j]);j++)
                s+=formula[j];
            t = newNode(s);
            operand.push(t);
            i=j-1;
        }

        else if(isLTL1(formula[i]))
        {
            ltl=newNode(formula[i]);
        }

        else if(isA1(formula[i]) || isE1(formula[i]))
        {
            int j;
            string s(1,formula[i]);
            i++;
            for(j=i;isIden1(formula[j]);j++)
                if(formula[j]!=' ')
                s+=formula[j];
            i=(j-1);
            t = newNode(s);
            optr.push(t);
        }

        else if(isOperator1(formula[i])||isNot1(formula[i]))
        {
            string s(1,formula[i]);
            if(formula[i]=='=' || formula[i]=='!')
                s+=formula[++i];
            t = newNode(s);

label:      t_op=optr.top();
            optr.pop();
            //cout<<"\nSuccessfully crossed label for "<<t_op->value[0]<<endl;
            if(!(formula[i]=='~'))
            if(!isOperator1(t_op->value[0]) && !isOpenParen1(t_op->value[0]))
            {
                if(isBinaryOp1(t_op->value))
                {
                    t1 = operand.top();
                    operand.pop();
                    t2 = operand.top();
                    operand.pop();
         
                    
                    t_op->right = t1;
                    t_op->left = t2;
                    //cout<<"\nBinary op\n";
         
                    
                    operand.push(t_op);
                }
                else if(isUnaryOp1(t_op->value))
                {
                    t1 = operand.top(); 
                    operand.pop();      

                    t_op->right = t1;
                    t_op->left = NULL;
                    //cout<<"\nUnary op\n";

                    operand.push(t_op);
                }
                goto label;   
            }
            optr.push(t_op);
            optr.push(t);
        }


        else if(isCloseParen1(formula[i]))
        {

            t_op = optr.top();
            optr.pop();

            //cout<<"\nTop value is "<<t_op->value<<endl;
            while(t_op->value!="(")
            {
                
                if(isBinaryOp1(t_op->value))
                {
                    t1 = operand.top(); 
                    operand.pop();      
                    t2 = operand.top();
                    operand.pop();
         
                    
                    t_op->right = t1;
                    t_op->left = t2;
                    
                    //cout<<"\nPushing in "<<t_op->value<<endl;
                    operand.push(t_op);
                }
                else if(isUnaryOp1(t_op->value))
                {
                    t1 = operand.top(); 
                    operand.pop();      

                    t_op->right = t1;
                    t_op->left = NULL;
                    
                    //cout<<"\nPushing in "<<t_op->value<<endl;
                    
                    operand.push(t_op);
                }
                t_op = optr.top();
                optr.pop();
                //cout<<"\nTop value is "<<t_op->value<<endl;
            }  
        }

        else continue;


    }
 
    
    t = operand.top();
    operand.pop();
 
    ltl->right=t;

    return ltl;
}





//-------------------------------------------------------------------------------------------
//											END OF SECOND VERSION
//-------------------------------------------------------------------------------------------




int main(int argc, char **argv)
{

    printf("here\n");
    FILE *Fp;
    char Formula[255];
    //tweak:make this scalable
    char PL[2055];
        printf("here\n");
    //char *formula;
    Fp = fopen(argv[1], "r");

    fgets(Formula, 255, (FILE*)Fp);
    loop:if(strstr(Formula,"MFOTLSPEC")==NULL)
        if(!feof(Fp))
        {
            fgets(Formula, 255, (FILE*)Fp);
            goto loop;
        }
    //tweak:make sure that the spec is present
    fgets(Formula, 255, (FILE*)Fp);
    printf("%s\n",Formula);
    //tweak:check for more formulas
    printf("%d",strlen(Formula));
    printf("here\n");
    char* formula = new char[(strlen(Formula))];
    strcpy(formula,Formula);
    printf("%s\n",formula);
    fclose(Fp);
        printf("here\n");
/*
	char* formula = (char*) malloc(100*sizeof(char));
	char* tmp_formula = (char*) malloc(100*sizeof(char));	
	do
	{	
		//printf("Enter the input choreography formula with proper paranthesis : \n");
		scanf("%s",formula);
		if(formula[0]!='(')
		{
			strcpy(tmp_formula,"(");
			strcat(tmp_formula, formula);
			strcat(tmp_formula,")");
			strcpy(formula,tmp_formula);
		}
	}while(!check_paranthesis(formula));
*/

//MFO formulas
//	char* formula="(Ex(p(x)))";//--output correct
//	char* formula="(~(Ex(p(x))))";//--output correct
//	char* formula="(Ex(~(p(x))))";//--output correct


//	char* formula="(Ax(p(x)))";//--output correct
//	char* formula="(~(Ax(p(x))))";//--output correct
//	char* formula="(Ay10(~(t1(y10))))";//--output correct

//	char* formula="(Ex((p(x))&(q(x))))";//--output correct
//	char* formula="((Ex(p(x)))&(Ey(q(y))))";//--output correct
//	char* formula="((Ex(p(x)))&(Ex(q(x))))";//--output correct

//	char* formula="(Ax((p(x))&(q(x))))";//--output correct
//	char* formula="((Ax(p(x)))&(Ay(q(y))))";//--output correct
//	char* formula="((Ax(p(x)))&(Ax(q(x))))";//--output correct

//	char* formula="((Ax(p(x)))&(Ey(p(y))))";//--output correct


//	char* formula="(Ax(Ey((p(x))&(p(y)))))";//--output wrong
//	char* formula="(Ex(Ay((p(x))&(p(y)))))";//--output wrong
//	char* formula="(Ax(Ay((p(x))&(p(y)))))";//--output wrong
//	char* formula="(Ex(Ey((p(x))&(p(y)))))";//--output wrong
//	char* formula="(Ex(Ey(p(x))))";//--output wrong
//	char* formula="(Ex(Ay(p(x))))";//--output wrong
//	char* formula="(Ax(Ey(p(x))))";//--output wrong



//MFO formulas with equality
	//char* formula="(Ex(Ey((x=y)&((p(x)|(p(y)))))))";//--tree construction has problems
//	char* formula="(Ex(Ey(x=y)))";//--output correct

//MFOTL formulas
//	char* formula="(G(Ex(p(x))))";
//	char* formula="(G((Ex(p(x)))&(Ex(q(x)))))";
//
//LTL formulas
//	char* formula="((G(F(p11)))&(~(G(p12))))";
//	char* formula="(~(G(p12)))";
// 	char* formula="(G(F(p)))";
// 	char* formula="(F(p))";
//	char* formula="(G(p))";
// 	char* formula="(G(F(X(p))))";

//PL formulas
//	char* formula="(((p1)&(p2))|((~(p2))&(p3)))";
//	char* formula="((p1)&(p2))";
//	char* formula="(p1)";

//-------------------------------------------------------------------------------------------
//											SECOND VERSION
//-------------------------------------------------------------------------------------------



//string formula1 = "( Ex1 (Ey1( (x1!=y1) & p(x1) | p(y1) )))";  //One rule is that the whole expression must be
    cout<<"\nbefore calling fn\n";
    string formula1(formula);                                                      //enclosed within parentheses
    tree* r = constructTree1(formula1);
    printf("inorder expression is \n");
    inorder1(r);


//-------------------------------------------------------------------------------------------
//											END OF SECOND VERSION
//-------------------------------------------------------------------------------------------
    //const char *formula = formula1.c_str();

	FTree* TFormula=NULL;
//	FTree* NTFormula=NULL;
//	FTree* JthFormula=NULL;

//	atom_set* AtomSet=NULL;

	int l,i,j;

/*
	////printf("\nEnter the propositional logic formula\n");
	scanf("%s",formula);
*/

//	////printf("\nYou have entered\n%s\n",formula);

	
//we are not able to read spaces which may be very helpful for input.

//convert the formula to formula tree.

//--------------------Initialization of formula_info structure.---------------------------------------
//--------------------PNum and VNUm are respectively 0 and the two lists PList and VList are empty.

	//Main C to C++ conversions
//T* a = (T*)malloc(sizeof(T)) becomes new T.
//T* b = (T*)malloc(N * sizeof(T)) becomes new T[N].
//free(a) becomes delete a.
//free(b) becomes delete[] b

	FInfo* formula_info = new FInfo;

	formula_info->PList=new List;
	formula_info->PList->size=0;
	formula_info->PList->first=NULL;
	formula_info->PList->last=NULL;

	formula_info->VList=new List;
	formula_info->VList->size=0;
	formula_info->VList->first=NULL;
	formula_info->VList->last=NULL;

	formula_info->PNum=0;
	formula_info->VNum=0;

	//TFormula=FTree_convert(formula,formula_info);
	construct_ftree(r,TFormula,formula_info);
    printf("\nFormula Tree Constructed...............\n");

	printf("\nThere are at most %d predicate symbols in the formula\n",formula_info->PNum);
	printf("\nThere are at most %d variable symbols in the formula\n",formula_info->VNum);

//	exit(0);
//----------------------read the formula tree and print the leaves----------------------------------------------------
	printf("\nReading the Formula Tree...............\n");
	printf("\nIn infix form:");
	display_FTree(TFormula);
	printf("\n");
	printf("\nIn prefix form:");
	display_FTree_pre(TFormula);
	printf("\n");
//	getchar();

//now we compute the set of predicates and variables occurring in the formula

//the predicate and variable lists are implemented as arrays within the formula_info structure

//first we allocate memory to the two lists
//	formula_info->PList=malloc(formula_info->PNum*sizeof(char));
//	if(formula_info->PList==NULL){
//		printf("\nMemory allocation error for predicate list\n");
//		exit;
//	}
//	printf("\nThe size of memory allocated to predicate list is %u",sizeof(formula_info->PList));
//	formula_info->VList=malloc(formula_info->VNum*sizeof(char));
//	if(formula_info->VList==NULL){
//		printf("\nMemory allocation error for variable list\n");
//		exit;
//	}
//	printf("\nThe size of memory allocated to variable list is %u",sizeof(formula_info->VList));
//then we initialize the PNum and VNum back to zero
//	formula_info->PNum=0;
//	formula_info->VNum=0;
	
	//printf("\nNow, we call the routine to construct models\n");
	fflush(stdout);
//	model_construct(TFormula,formula_info);	

//	printf("\nWe have constructed the models\n");
	fflush(stdout);
//let us print the predicates and variables in the formula
	List_node* Lt=NULL;
	i=0;
	printf("\nThe no. of variables is: %d",formula_info->VNum);

	Lt=formula_info->VList->first;
	if(Lt!=NULL && formula_info->VNum!=0){
	 do{

		printf("\nThe %dth variable in the formula -> %s\n",++i,Lt->val);
		Lt=Lt->right;
	 }while(Lt!=formula_info->VList->first);
	}
	else if(Lt==NULL && formula_info->VNum==0){
		printf("\nNo Predicates in the VList.\nContinue.");

	}

	else{
		printf("\nThere is some problem with the VList.\nPlease check.\nExiting....................");
		exit(1);
	}


	i=0;
	printf("\nThe no. of predicates is: %d",formula_info->PNum);

// if formula_info->PNum is ZERO and Lt is NOT NULL there is a problem.
//also, if formula_info->PNum is NON ZERO and Lt is NULL there is a problem.
	Lt=formula_info->PList->first;
	if(Lt!=NULL && formula_info->PNum!=0){
	 do{

		printf("\nThe %dth predicate in the formula -> %s\n",++i,Lt->val);
		Lt=Lt->right;
	 }while(Lt!=formula_info->PList->first);
	}
	else if(Lt==NULL && formula_info->PNum==0){
		printf("\nNo Predicates in the PList.\nContinue.");

	}
	else{
		printf("\nThere is some problem with the PList.\nPlease check.\nExiting....................");
		exit(1);
	}

//	return 0;


	unsigned int M=1 << formula_info->PNum;
//	unsigned int R=1 << formula_info->VNum;

	formula_info->M=M;
	formula_info->R=formula_info->VNum;


	//at this point, we know the number of predicates (M=formula_info->PNum), the number of variables (K=formula_info->VNum), the variables (formula_info->VList), the predicates (formula_info->PList).
	TFormula=eliminate_quantifier(TFormula,formula_info);
	printf("The formula with all quantifiers eliminated is as follows:\nIn prefix form:");
	display_FTree_pre_sub(TFormula);
	printf("\n");

	printf("\nIn infix form:");
	display_FTree_sub(TFormula);
	printf("\n");

	    //Final year stuff begins.......
    //
    int no_of_clients=0;
    printf("here\n");
    rename(argv[1],"temp.txt");
    printf("here\n");
    Fp = fopen("temp.txt", "r");
    printf("here\n");
    FILE *fp;
    fp = fopen("temp1.txt","a");
    printf("here\n");
    fgets(Formula, 255, (FILE*)Fp);
    printf("here\n");
    loopx:
    printf("here in loopx %d is no of clients\n",no_of_clients);
    if(strstr(Formula,"VAR")!=NULL&&!feof(Fp))
    {
        cout<<"\nHere1\n";
        fputs(Formula,fp);
        fgets(Formula, 255, (FILE*)Fp);
        loopy:
        if(strstr(Formula,"ASSIGN")!=NULL)
        {
            printf("FOund assign\n");
            char var_dec_p[100];
            char var_dec_q[100];
            char var_dec_ctr[100];
            char buffer[4];
            char types[4];

            sprintf(buffer,"%d",formula_info->M*formula_info->R);
            printf("read from tree %d\n",formula_info->M*formula_info->R);
            sprintf(types,"%d",no_of_clients);
            printf("read clients %d\n",no_of_clients);

            if(no_of_clients!=1){
            strcpy(var_dec_p,"VAR p : array 1..");
            strcpy(var_dec_q,"q : array 1..");
            //strcpy(var_dec_ctr,"ctr: 1..");

            printf("\nCopied\n");

            strcat(var_dec_p,types);
            strcat(var_dec_q,types);
            //strcat(var_dec_ctr,types);

            printf("\nconcat 1\n");

            strcat(var_dec_p," of array 1..");
            strcat(var_dec_q," of array 1..");
            //strcat(var_dec_ctr," of array 1..");
        }
        else
        {

            strcpy(var_dec_p,"VAR p : array 1..");
            strcpy(var_dec_q,"q : array 1..");
            //strcpy(var_dec_ctr,"ctr: 1..");

            printf("\nCopied\n");
        }
            printf("\nconcat 2\n");

            strcat(var_dec_p,buffer);
            strcat(var_dec_q,buffer);
            //strcat(var_dec_ctr,buffer);

            printf("\nconcat 3\n");

            strcat(var_dec_p," of boolean;\n");
            strcat(var_dec_q," of boolean;\n");
            //strcat(var_dec_ctr,";\n");

            printf("\nconcat 3\n");

            fputs(var_dec_p,fp);
            fputs(var_dec_q,fp);
            //fputs(var_dec_ctr,fp);
            if(no_of_clients!=1)
            for(int x=1;x<=no_of_clients;x++)
            {
                fputs("ctr[",fp);
                char index[4];
                sprintf(index,"%d",x);
                fputs(index,fp);
                fputs("]: 0..",fp);
                fputs(buffer,fp);
                fputs(";\n",fp);
            }
            else
            {
                fputs("ctr: 0..",fp);
                fputs(buffer,fp);
                fputs(";\n",fp);
            }
            fputs(Formula,fp);
            fgets(Formula, 255, (FILE*)Fp);

            printf("Gets and puts\n");

            goto loopx;
        }
        else if(strstr(Formula,"ip")!=NULL)
        {
            bool Flag=false;
            printf("\nfound ip\n");
            for(int i=0;i<strlen(Formula)-3;i++)
                if(Formula[i]=='1'&&Formula[i+1]=='.'&&Formula[i+2]=='.')
                    {
                        for(int j=i+3,k=0;Formula[j]!=' ';j++,k++)
                        {
                            int temp = Formula[j]-48;
                            no_of_clients+=temp*pow(10,k);
                            //no_of_clients=Formula[j];
                        }
                        Flag=true;
                        break;
                    }
            if(!Flag)
                no_of_clients=1;
            printf("\nOut of found ip loop\n");
            fputs(Formula,fp);
            fgets(Formula, 255, (FILE*)Fp);
            goto loopy; 
        }
        else
        {
            printf("\nGoing into else\n");
            fputs(Formula,fp);
            fgets(Formula, 255, (FILE*)Fp);
            goto loopy; 
        }
    }

    if(strstr(Formula,"MFOTLSPEC")==NULL&&strstr(Formula,"VAR")==NULL&&!feof(Fp))
    {
                cout<<"\nHere2\n";
        fputs(Formula,fp);
        fgets(Formula, 255, (FILE*)Fp);
        goto loopx;
    }
    if(strstr(Formula,"MFOTLSPEC")!=NULL)//Prints LTL to file
    {
        
                cout<<"\nHere3\n";
        if(no_of_clients!=1)
        for(int t=1;t<=no_of_clients;t++)
        {
            fputs("init(ctr[",fp);
            char num[4];
            sprintf(num,"%d",t);
            fputs(num,fp);
            fputs("]):= 0;\n",fp);//have to do this also

        }
        else
        {
            fputs("init(ctr):= 0;\n",fp);
        }
        cout<<"\nHere01\n";
        if(no_of_clients!=1)
        for(int t=0;t<no_of_clients;t++)
        for(int i=0;i<formula_info->M*formula_info->R;i++)
        {
            char num[4];
            char num1[4];
            char text[28];
            sprintf(num,"%d",i+1);
            sprintf(num1,"%d",t+1);
            strcpy(text,"init(p[");
            strcat(text,num1);
            strcat(text,"][");
            strcat(text,num);
            strcat(text,"]):= FALSE;\n");
            fputs(text,fp);
        }
        else
            //for(int t=0;t<no_of_clients;t++)
            for(int i=0;i<formula_info->M*formula_info->R;i++)
            {
                char num[4];
                char num1[4];
                char text[28];
                sprintf(num,"%d",i+1);
                //sprintf(num1,"%d",t+1);
                strcpy(text,"init(p[");
                //strcat(text,num1);
                //strcat(text,"][");
                strcat(text,num);
                strcat(text,"]):= FALSE;\n");
                fputs(text,fp);
            } 
        cout<<"\nHere02\n";
        fputs("\n",fp);
        if(no_of_clients!=1)
            for(int t=0;t<no_of_clients;t++)
            for(int i=0;i<formula_info->M*formula_info->R;i++)
            {
                char num[4];
                char num1[4];
                char text[28];
                sprintf(num,"%d",i+1);
                sprintf(num1,"%d",t+1);
                strcpy(text,"init(q[");
                strcat(text,num1);
                strcat(text,"][");
                strcat(text,num);
                strcat(text,"]):= FALSE;\n");
                fputs(text,fp);
            }
        else
            //for(int t=0;t<no_of_clients;t++)
            for(int i=0;i<formula_info->M*formula_info->R;i++)
            {
                char num[4];
                char num1[4];
                char text[28];
                sprintf(num,"%d",i+1);
                //sprintf(num1,"%d",t+1);
                strcpy(text,"init(q[");
                //strcat(text,num1);
                //strcat(text,"][");
                strcat(text,num);
                strcat(text,"]):= FALSE;\n");
                fputs(text,fp);
            }
        fputs("\n",fp);
                cout<<"\nHere03\n";

        if(no_of_clients!=1)
        for(int t=1;t<=no_of_clients;t++)
        {
            fputs("next(ctr[",fp);
            char num[4];
            sprintf(num,"%d",t);
            fputs(num,fp);
            fputs("]):= case\n",fp);
            fputs("\tip[",fp);
            fputs(num,fp);
            fputs("]=TRUE   & ctr[",fp);
            //char num[4];
            //sprintf(num,"%d",t+1);
            fputs(num,fp);
            fputs("] < ",fp);
            char buffer[4];
            sprintf(buffer,"%d",formula_info->M*formula_info->R);
            fputs(buffer,fp);
            fputs(" : ctr[",fp);
            fputs(num,fp);
            fputs("] + 1;\n",fp);
            fputs("\tip[",fp);
            fputs(num,fp);
            fputs("]=FALSE   & ctr[",fp);
            fputs(num,fp);
            fputs("] >0 : ctr[",fp);
            fputs(num,fp);
            fputs("] - 1;\n",fp);


            fputs("\tTRUE\t: ctr[",fp);
            fputs(num,fp);
            fputs("];\nesac;\n\n",fp);

        }
        else
        {
            fputs("next(ctr",fp);
            char num[4];
            //sprintf(num,"%d",t);
            //fputs(num,fp);
            fputs("):= case\n",fp);
            fputs("\tip",fp);
            //fputs(num,fp);
            fputs("=TRUE   & ctr < ",fp);
            //char num[4];
            //sprintf(num,"%d",t+1);
            //fputs(num,fp);
            //fputs("] < ",fp);
            char buffer[4];
            sprintf(buffer,"%d",formula_info->M*formula_info->R);
            fputs(buffer,fp);
            fputs(" : ctr",fp);
            //fputs(num,fp);
            fputs(" + 1;\n",fp);
            fputs("\tip",fp);
            //fputs(num,fp);
            fputs("=FALSE   & ctr",fp);
            //fputs(num,fp);
            fputs(" >0 : ctr",fp);
            //fputs(num,fp);
            fputs(" - 1;\n",fp);


            fputs("\tTRUE\t: ctr",fp);
            //fputs(num,fp);
            fputs(";\nesac;\n\n",fp);   
        }
        //fputs("next(ctr):= case\n",fp); 
        //fputs("\tip=TRUE   & ctr <",fp);
        //char buffer[4];
        //sprintf(buffer,"%d",formula_info->M*formula_info->R);
        //fputs(buffer,fp);
        //fputs(" : ctr + 1;\n",fp);
        //fputs("\tip=FALSE   & ctr >1 : ctr - 1;\n",fp);
        //fputs("\tTRUE\t: ctr;\nesac;\n\n",fp);
        cout<<"\nHere04\n";
        if(no_of_clients!=1)
        for(int t=0;t<no_of_clients;t++)
        for(int i=0;i<formula_info->M*formula_info->R;i++)
        {
            fputs("next(p[",fp);
            char buff1[4];
            char buff2[4];
            char spl[4];

            sprintf(buff1,"%d",i+1);
            sprintf(buff2,"%d",t+1);
            sprintf(spl,"%d",i);
            //sprintf(buff2,"%d",formula_info->M*formula_info->R);
            fputs(buff2,fp);
            fputs("][",fp);
            fputs(buff1,fp);
            fputs("]):= case\n",fp);
            fputs("\tp[",fp);
            fputs(buff2,fp);
            fputs("][",fp);
            fputs(buff1,fp);
            fputs("]=FALSE & ip[",fp);
            fputs(buff2,fp);
            fputs("] = TRUE & ctr[",fp);
            fputs(buff2,fp);
            fputs("] = ",fp);
            fputs(spl,fp);
            fputs(" : TRUE;\n",fp);

            fputs("\tq[",fp);
            fputs(buff2,fp);
            fputs("][",fp);
            fputs(buff1,fp);
            fputs("]=TRUE ",fp);
            //fputs(buff1,fp);
            fputs(" : FALSE;\n\tTRUE\t: p[",fp);
            fputs(buff2,fp);
            fputs("][",fp);
            fputs(buff1,fp);
            fputs("];\nesac;\n\n",fp);
            //---------------------TEST-------//

            
            //char buff1[4];
            //char buff2[4];

            //sprintf(buff1,"%d",i+1);
            //sprintf(buff2,"%d",formula_info->M*formula_info->R);
            //fputs(buff1,fp);
            //fputs("]):= case\n",fp);
            

        }

        else
        //for(int t=0;t<no_of_clients;t++)
        for(int i=0;i<formula_info->M*formula_info->R;i++)
        {
            fputs("next(p",fp);
            char buff1[4];
            char buff2[4];
            char spl[4];
            sprintf(buff1,"%d",i+1);
            //sprintf(buff2,"%d",t+1);
            sprintf(spl,"%d",i);
            //sprintf(buff2,"%d",formula_info->M*formula_info->R);
            //fputs(buff2,fp);
            fputs("[",fp);
            fputs(buff1,fp);
            fputs("]):= case\n",fp);
            fputs("\tp[",fp);
            //fputs(buff2,fp);
            //fputs("][",fp);
            fputs(buff1,fp);
            fputs("]=FALSE & ip",fp);
            //fputs(buff1,fp);
            fputs(" = TRUE & ctr = ",fp);
            //fputs(buff2,fp);
            //fputs("] = ",fp);
            fputs(spl,fp);
            fputs(" : TRUE;\n",fp);

            fputs("\tq[",fp);
            //fputs(buff2,fp);
            //fputs("][",fp);
            fputs(buff1,fp);
            fputs("]=TRUE ",fp);
            //fputs(buff1,fp);
            fputs(" : FALSE;\n\tTRUE\t: p[",fp);
            //fputs(buff2,fp);
            //fputs("][",fp);
            fputs(buff1,fp);
            fputs("];\nesac;\n\n",fp);
            //---------------------TEST-------//

            
            //char buff1[4];
            //char buff2[4];

            //sprintf(buff1,"%d",i+1);
            //sprintf(buff2,"%d",formula_info->M*formula_info->R);
            //fputs(buff1,fp);
            //fputs("]):= case\n",fp);
            

        }

        cout<<"\nHere05\n";

        if(no_of_clients!=1)
        for(int t=0;t<no_of_clients;t++)
        for(int i=0;i<formula_info->M*formula_info->R;i++)
        {
            fputs("next(q[",fp);
            char buff1[4];
            char buff2[4];

            sprintf(buff1,"%d",i+1);
            sprintf(buff2,"%d",t+1);

            //sprintf(buff2,"%d",formula_info->M*formula_info->R);
            fputs(buff2,fp);
            fputs("][",fp);
            fputs(buff1,fp);
            fputs("]):= case\n",fp);
            fputs("\tq[",fp);
            fputs(buff2,fp);
            fputs("][",fp);
            fputs(buff1,fp);
            fputs("]=FALSE & ip[",fp);
            fputs(buff2,fp);
            fputs("] = FALSE & ctr[",fp);
            fputs(buff2,fp);
            fputs("] = ",fp);
            fputs(buff1,fp);
            fputs(" : TRUE;\n",fp);
            fputs("\tq[",fp);
            fputs(buff2,fp);
            fputs("][",fp);
            fputs(buff1,fp);
            fputs("]=TRUE ",fp);
            //fputs(buff1,fp);
            fputs(" : FALSE;\n\tTRUE\t: q[",fp);
            fputs(buff2,fp);
            fputs("][",fp);
            fputs(buff1,fp);
            fputs("];\nesac;\n\n",fp);
            //---------------------TEST-------//

            

        }

        else
        {
            for(int t=0;t<no_of_clients;t++)
            for(int i=0;i<formula_info->M*formula_info->R;i++)
            {
                fputs("next(q[",fp);
                char buff1[4];
                char buff2[4];

                sprintf(buff1,"%d",i+1);
                sprintf(buff2,"%d",t+1);

                //sprintf(buff2,"%d",formula_info->M*formula_info->R);
                //fputs(buff2,fp);
                //fputs("][",fp);
                fputs(buff1,fp);
                fputs("]):= case\n",fp);
                fputs("\tq[",fp);
                //fputs(buff2,fp);
                //fputs("][",fp);
                fputs(buff1,fp);
                fputs("]=FALSE & ip",fp);
                //fputs(buff1,fp);
                fputs(" = FALSE & ctr",fp);
                //fputs(buff2,fp);
                fputs(" = ",fp);
                fputs(buff1,fp);
                fputs(" : TRUE;\n",fp);
                fputs("\tq[",fp);
                //fputs(buff2,fp);
                //fputs("][",fp);
                fputs(buff1,fp);
                fputs("]=TRUE ",fp);
                //fputs(buff1,fp);
                fputs(" : FALSE;\n\tTRUE\t: q[",fp);
                //fputs(buff2,fp);
                //fputs("][",fp);
                fputs(buff1,fp);
                fputs("];\nesac;\n\n",fp);
                //---------------------TEST-------//
            } 
        }
        cout<<"\nHere06\n";
        fputs("LTLSPEC\n",fp);
        cout<<"\nHere07\n";
        FILE *fp1;
        cout<<"\nHere08\n";
        fp1 = fopen("LTLSPEC.txt","r");
        cout<<"\nHere09\n";
        fgets(PL, 2056, (FILE*)fp1);
        cout<<"\nHere10\n";
        fputs(PL,fp);
        cout<<"\nHere11\n";
    }
    
            cout<<"\nHere5\n";
    fclose(Fp);
    cout<<"\nHelloooooo\n";
    char *file_name = new char[MAX];
    cout<<"\nHelloooooo\n";
    strncpy(file_name, argv[1], strlen(argv[1])-4);
    file_name[strlen(file_name)]='\0';
    strcat(file_name,"_output.smv");
    rename("temp.txt",argv[1]);
    rename("temp1.txt",file_name);
    remove("LTLSPEC.txt");
    return 0;

//------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------
/*	subf_construct(TFormula);

//after constructing the closure set we need to see whether it contains all the positive and negative formulas that we expect.
//thereafter we construct the states each of which are subset of formulas in the closure set.
	if(i_subf!=i_neg_subf){
		//printf("Error in subformula set construction. Check...");
		exit(1);
	}

	int k=i_subf;
	int max_k;//is the number of subsets of closure set
	max_k=ll0_pow(2,k)-1;

	for(i=0;i<k;i++){
		//printf("\nthe %d th formula in closure set of positive formulas\t",i+1);
		//printf("in the prefix form\t");
		//display_FTree_pre_sub(subf[i]);
	}
	//printf("\n");
	for(i=0;i<k;i++){
		//printf("\nthe %d th formula in closure set of negative formulas\t",i+1);
		//printf("in the prefix form\t");
		//display_FTree_pre_sub(neg_subf[i]);
	}
	//printf("\n");
	//getchar();
	k=i_untilf;//is the number of until formulas
	for(i=0;i<k;i++){
		//printf("\nthe %d th formula in set of until (diamond) formulas\t",i+1);
		//printf("in the prefix form\t");
		//display_FTree_pre_sub(untilf[i]);
	}
	//printf("\n");
	//getchar();
	k=i_nextf;//is the number of until formulas
//	int max_k;//is the number of subsets of U
//	max_k=ll0_pow(2,k)-1;
	for(i=0;i<k;i++){
		//printf("\nthe %d th formula in set of next formulas\t",i+1);
		//printf("in the prefix form   ");
		//display_FTree_pre_sub(nextf[i]);
	}
	//printf("\n");

//	return 1;
//--------------------------------------------------------
	AtomSet=atom_construct(TFormula);

//Now look at the AtomSet again and print the valid atoms.
	//getchar();
	atom_node* At=NULL;
	At=AtomSet->first;
	//printf("\nThere are %d valid atoms",AtomSet->size);
	do{
		//printf("\n(%d,%d) is a valid atom",At->atom_val,max_k-At->atom_val);
		At=At->right;
	}while(At!=AtomSet->first);
	//printf("\n");
	//done
	//now construct the set of states
	state_set* StateSet=NULL;

	StateSet=state_construct(AtomSet);
	//printf("\nSet of States constructed successfully.\n");
//set of states constructed, i believe, successfully.
	fflush(stdout);
//print the state set
	//getchar();
	//printf("\nThere are %d valid states",StateSet->size);
	fflush(stdout);
	state_node* St=NULL;
	St=StateSet->first;
	do{
		//printf("\n(%d,%d) is a valid state",St->sr->atom_val,St->sr->untl_val);
		St=St->right;
	}while(St!=StateSet->first);
	printf("\n");
//	return 1;
//construct the set of initial states
//this can be done while constructing the state set, isn't it?
//construct the set of final states too, simultaneously.

	state_set* InitStates=NULL;
	state_set* FinalStates=NULL;
	InitStates=malloc(sizeof(state_set));
	if(InitStates==NULL){
		//printf("\nError in Memory Allocation for Initial State Set\n");
		exit(1);
	}
	FinalStates=malloc(sizeof(state_set));
	if(FinalStates==NULL){
		//printf("\nError in Memory Allocation for Final State Set\n");
		exit(1);

	}
	InitStates->size=0;
	InitStates->first=NULL;
	InitStates->last=NULL;

	FinalStates->size=0;
	FinalStates->first=NULL;
	FinalStates->last=NULL;

//	state_node* St=NULL;
	St=StateSet->first;
//	//printf("\nLooking at the first state.\n");
	do{
//if St is an inital state, add it to the InitStates set.
		if(St->sr->init==1 && St->sr->untl_val==0){
			printf("\n(%d,%d) is an initial state.",St->sr->atom_val,St->sr->untl_val);
			fflush(stdout);
			add_to_state_set(InitStates,St);
		}
//if St is a final state, add it to the FinalStates set.
		if(St->sr->untl_val==0){
			//printf("\n(%d,%d) is a final state.",St->sr->atom_val,St->sr->untl_val);
			fflush(stdout);
			add_to_state_set(FinalStates,St);
		}
//move to the next state.
		St=St->right;
//		//printf("\nMove to the next state.\n");
	}while(St!=StateSet->first);
	printf("\n");
	//getchar();
/*
	//printf("\nThere are %d valid intial states",InitStates->size);
	St=InitStates->first;
	do{
		//printf("\n(%d,%d) is a valid inital state",St->sr->atom_val,St->sr->untl_val);
		St=St->right;
	}while(St!=InitStates->first);
	//printf("\n");

	//printf("\nThere are %d valid final states",FinalStates->size);
	St=FinalStates->first;
	do{
		//printf("\n(%d,%d) is a valid inital state",St->sr->atom_val,St->sr->untl_val);
		St=St->right;
	}while(St!=FinalStates->first);
	//printf("\n");
*/
/*
//now construct the set of transitions, again as a doubly linked list.
	transn_set* TransnSet=NULL;
	TransnSet=transn_construct(StateSet);
//print the transition set
	transn_node* Tr=NULL;
	//printf("\nThere are %d transition pairs",TransnSet->size);
	Tr=TransnSet->first;
	do{
		//printf("\n<(%d,%d)->(%d,%d)>",Tr->tr->sr1->atom_val,Tr->tr->sr1->untl_val,Tr->tr->sr2->atom_val,Tr->tr->sr2->untl_val);
		Tr=Tr->right;
	}while(Tr!=TransnSet->first);
	printf("\n");
	//getchar();


//-----------------------------------------------------------------------------------------------------------------------------
//	Reducing the automata
//-----------------------------------------------------------------------------------------------------------------------------

	state_set* ReducedStates=NULL;
	ReducedStates=malloc(sizeof(state_set));
	if(ReducedStates==NULL){
		//printf("\nError in Memory Allocation for Initial State Set\n");
		exit(1);
	}
	St=InitStates->first;
	do{
//		//printf("\n(%d,%d) is a valid inital state",St->sr->atom_val,St->sr->untl_val);
		add_to_state_set(ReducedStates,St);
		St=St->right;
	}while(St!=InitStates->first);
	
	St=ReducedStates->first;
	do
	{
//		//printf("\n(%d,%d) is a valid state in reduced automata",St->sr->atom_val,St->sr->untl_val);
		Tr=TransnSet->first;
		do
		{
			if((St->sr->atom_val == Tr->tr->sr1->atom_val) && (St->sr->untl_val == Tr->tr->sr1->untl_val))
			{
				state_node* tsn=NULL;
				tsn = find_state(StateSet,Tr->tr->sr2);
				if(find_state(ReducedStates,tsn->sr)==NULL)
				{
//					//printf("\n(%d,%d) is a valid state",tsn->sr->atom_val,tsn->sr->untl_val);
//					//getchar();
					add_to_state_set(ReducedStates,tsn);
				}
			}
			Tr=Tr->right;
		}while(Tr!=TransnSet->first);
		St=St->right;
//		//getchar();
	}while(St!=ReducedStates->first);

	printf("\nThere are %d valid states in reduced automata",ReducedStates->size);
	St=ReducedStates->first;
	do
	{
		printf("\n(%d,%d) is a valid state in reduced automata",St->sr->atom_val,St->sr->untl_val);
		St=St->right;
	}while(St!=ReducedStates->first);
			
	printf("\n");
	
	transn_set* ReducedTransn=NULL;
	ReducedTransn=malloc(sizeof(transn_set));
	if(ReducedTransn==NULL){
		////printf("\nError in Memory Allocation\n");
		exit(1);
	}
	ReducedTransn->size=0;
	ReducedTransn->first=NULL;
	ReducedTransn->last=NULL;
	Tr=TransnSet->first;
	do
	{
		state_node* tsn1=NULL;
		state_node* tsn2=NULL;
		tsn1 = find_state(ReducedStates,Tr->tr->sr1);
		tsn2 = find_state(ReducedStates,Tr->tr->sr2);
		if(tsn1 !=NULL && tsn2 !=NULL)
		{
			add_to_transn_set(ReducedTransn,tsn1,tsn2);
//			//printf("\n<(%d,%d)->(%d,%d)>",Tr->tr->sr1->atom_val,Tr->tr->sr1->untl_val,Tr->tr->sr2->atom_val,Tr->tr->sr2->untl_val);
		}
		Tr=Tr->right;
	}while(Tr!=TransnSet->first);

	printf("\nThere are %d transition pairs in reduced automata",ReducedTransn->size);
	Tr=ReducedTransn->first;
	do{
		printf("\n<(%d,%d)->(%d,%d)>",Tr->tr->sr1->atom_val,Tr->tr->sr1->untl_val,Tr->tr->sr2->atom_val,Tr->tr->sr2->untl_val);
		Tr=Tr->right;
	}while(Tr!=ReducedTransn->first);

	printf("\n");
*/
	return 1;

}//end of main


