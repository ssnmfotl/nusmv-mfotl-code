#include "mfotlFA.h"


////Main C to C++ conversions
//T* a = (T*)malloc(sizeof(T)) becomes new T.
//T* b = (T*)malloc(N * sizeof(T)) becomes new T[N].
//free(a) becomes delete a.
//free(b) becomes delete[] b


//----------------------------------------------------------------------------------------------------------------------------------------
FTree* FTree_convert(char psi[MAX],FInfo* formula_info)
{
	int len=0;
	int i,j;
	char token[20];
	char sub_psi[20];
	int done=-1;

	FTree* for_node_n=NULL;
	FTree* for_node_l=NULL;
	FTree* for_node_r=NULL;
	FTree* for_node=NULL;

	FTree* op_node=NULL;
	FTree* new_op_node=NULL;

	len=strlen(psi);
	printf("\nLength of the formula is %d\n",len);
	i=0;
	while(i<len)
	{
		printf("\nLooking at the %dth symbol of the formula:%c",i,psi[i]);
		//looking at psi[i]
		switch(isoperator(psi[i])){//we expect operators to be single letter symbols. curently, this is what is is.
					//for global logics opearators can't be single letters but multiple, viz, Pj, Xj etc.
		 case 1:
			printf("\n%c is an operator",psi[i]);
			switch(prcd(psi[i])){
				case 0://**(** do nothing
					printf("\nWe are looking at the symbol %c\n",psi[i]);
					printf("\nIgnore, move to the next symbol\n");
					++i;
					continue;
				case 1://**)**
					printf("\nWe are looking at the symbol %c\n",psi[i]);
					//getchar();
//-----------------------------------------------------------------------------------------------------------------------------
//")" can be encountered in three different places 1. p(x) 2. (x=y) 3. (\alpha op \beta) where op can be | or & or (~\alpha)
//we consider the third case here. second case is considered elsewhere.
//-----------------------------------------------------------------------------------------------------------------------------
					if(psi[i-1]==')'){
			//look at the operator on the top of the operation stack and accordingly construct a tree by taking one or more operands from the operand stack
						op_node=op_pop();
						printf("\npop an operator from the op stack\ncheck whether it is unary or binary\n ");
						fflush(stdout);
						printf("\nThe operator is %s",op_node->val);
						fflush(stdout);
						switch(strchk(op_node->val)){
							case 1://unary operator
								printf("\nif unary, pop a subformula from the formula stack\n");
								for_node_n=for_pop();
								display_FTree(for_node_n);
								op_node->next=for_node_n;
								op_node->type=1;//for unary operator
								op_node->model=-1;//
								op_node->copy=-1;//
								op_node->subd=0;//
								op_node->num=-1;
								
								printf("\nConstructed a larger subformula\n");
								display_FTree(op_node);
								printf("\npush the new subformula node onto the formula stack...\n");
								for_push(op_node);
								//display_for_stack();

								i++;
								continue;


							case 2://binary operator
								printf("\nif binary, pop two subformulas from the formula stack\n");
								for_node_r=for_pop();
								for_node_l=for_pop();
								op_node->left=for_node_l;
								op_node->right=for_node_r;
								op_node->type=2;//for binary operator
								op_node->model=-1;//
								op_node->copy=-1;//
								op_node->subd=0;//
								op_node->num=-1;
								
								printf("\npush the new subformula node onto the formula stack...\n");
								for_push(op_node);
								//display_for_stack();

								i++;
								continue;
							case 3://quantifiers
								printf("\nif quantifier, pop a subformula from the formula stack\n");
								for_node_n=for_pop();
								//display_FTree(for_node_n);
								op_node->next=for_node_n;
								op_node->type=3;//for quantifiers
								op_node->model=-1;//
								op_node->copy=-1;//
								op_node->subd=0;//
								op_node->num=-1;

								printf("\nConstructed a larger subformula\n");
								//display_FTree(op_node);
								printf("\npush the new subformula node onto the formula stack...\n");
								for_push(op_node);
								//display_for_stack();

								i++;
								continue;
							case 4://predicates
								printf("\nif predicate, pop a subformula from the formula stack\n");
								for_node_n=for_pop();
								//display_FTree(for_node_n);
								op_node->next=for_node_n;
								op_node->type=4;//for quantifiers
								op_node->model=-1;//
								op_node->copy=-1;//
								op_node->subd=0;//
								op_node->num=-1;


								printf("\nConstructed a larger subformula\n");
								//display_FTree(op_node);
								printf("\npush the new subformula node onto the formula stack...\n");
								for_push(op_node);
								//display_for_stack();

								i++;
								continue;
								


						}//end of switch

					
					}//end of if
					else{//do nothing
					//this is the case when we encounter closiing paranethesis of (x=y)
						printf("\nIgnored the %dth symbol %c\n", i,psi[i]);
						i++;
						continue;
					}

				case 2:	//if psi[i] is not left or right bracket but an operator (~,&,|, etc.) then construct an internal node and store in the stack, it is an operator proper ~,&,|, et. al.
					printf("\nLooking at a real operator %c",psi[i]);
					fflush(stdout);
					new_op_node=new FTree;
					if(new_op_node==NULL){
						printf("\nMemory Allocation Error\n");
						exit(1);
					}
					printf("\nConstructed a tree node to store %c with pointer %p",psi[i],new_op_node);
					fflush(stdout);

//					new_op_node->global=0;
					new_op_node->val[0]=psi[i];
					new_op_node->model=-1;
					new_op_node->copy=-1;
					new_op_node->subd=0;
					new_op_node->num=-1;

					switch(strchk(new_op_node->val)){
						case 1: new_op_node->type=1;//not a leaf
							new_op_node->val[1]='\0';
							i++;
							break;
							
						case 2: new_op_node->type=2;//not a leaf
							new_op_node->val[1]='\0';
							i++;
							break;

						case 3: new_op_node->type=3;
							j=1;
							while(psi[i+j]!='('){
								new_op_node->val[j]=psi[i+j];
								j++;
							}
							new_op_node->val[j]='\0';
							i=i+j;
							break;

						case 4: new_op_node->type=4;
							j=1;
							while(psi[i+j]!='('){
								new_op_node->val[j]=psi[i+j];
								j++;
							}
							new_op_node->val[j]='\0';

							done=add_predicate_to_formula_info(new_op_node->val,formula_info);
							if(done)
								formula_info->PNum++;
							i=i+j;
							break;

					}
					printf("\nLabel for internal node is %s",new_op_node->val);
					fflush(stdout);

					new_op_node->parent=NULL;
					new_op_node->right=NULL;
					new_op_node->left=NULL;
					new_op_node->next=NULL;
					new_op_node->model=-1;
					new_op_node->copy=-1;
					new_op_node->subd=0;
					new_op_node->num=-1;
				
					op_push(new_op_node);
					printf("\npush the new op node onto the stack...............\n");
					//display_op_stack();
					continue;

			
			}//end of outer switch
		 	continue;
//-------------------end of case 1 for isoperator----------------------------------------------------
		 case 0: //psi[i] is not an operator
			printf("\n%c is not an operator",psi[i]);
			fflush(stdout);

			//construct token
			if(psi[i-1]=='('||psi[i-1]=='='){
				token[0]='\0';//initialize the token if the preceding character is '('
				printf("\nInitialized the token.\nWe have to read the full variable.\n");
				fflush(stdout);
			}
			else{
				printf("\nThere is some problem with the structure of the input. Exiting.......");
				fflush(stdout);
				exit(1);
			}
			//printf("\nwe are here.1\n");
			//fflush(stdout);

			j=i;
			while(psi[j]!='='&& psi[j]!=')' ){
				token[j-i]=psi[j];
				j++;
				printf("\nAdding %c to the token\n",psi[j-1]);
				//getchar();
				printf("\nwe are here.\n");
				fflush(stdout);

			}
			token[j-i]='\0';

			//printf("\nwe are here.2\n");
			//fflush(stdout);

//
//token done
			for_node=new FTree;
			if(for_node==NULL){
				printf("\nMemory Allocation Error\n");
				exit(1);
			}
			for_node->type=0;
			strcpy(for_node->val,token);
			printf("\nwe are here.\n");
			fflush(stdout);
			done=add_variable_to_formula_info(for_node->val,formula_info);
			printf("\nwe are here.\n");
			fflush(stdout);
			if(done){
				printf("\nSuccessfully added a variable to the VList.\n"); 
				formula_info->VNum++;
			}
			printf("\nThe label for leaf node is %s with pointer %p\n",for_node->val,for_node);
			fflush(stdout);
			for_node->parent=NULL;
			for_node->right=NULL;
			for_node->left=NULL;
			for_node->next=NULL;

			for_node->model=-1;
			for_node->copy=-1;
			for_node->subd=0;
			for_node->num=-1;

			for_push(for_node);
			printf("\npush the new leaf node onto the stack...............\n");
			fflush(stdout);
					
			//at this point, we construct a subtree rooted at "=" as a special case
			if(psi[i-1]=='=' && psi[j]==')')
			{
				op_node=op_pop();
				printf("\npop = operator from the op stack\n ");
				fflush(stdout);
				printf("\nThe operator is %s\n",op_node->val);
				fflush(stdout);

				for_node_r=for_pop();
				for_node_l=for_pop();
				op_node->left=for_node_l;
				op_node->right=for_node_r;
				op_node->type=2;//for binary operator
				op_node->model=-1;//
				op_node->copy=-1;//
				op_node->subd=0;//
				op_node->num=-1;

				printf("\npush the new subformula node onto the formula stack ...............\n");
				for_push(op_node);


				i=j+1;
				continue;
			}
			else{//we are yet to see "=". the variable may be part of monadic predicate.
				i=j;//skip to the next character in the input after the variable
				continue;
			}
//-------------------end of case 0 for isoperator----------------------------------------------------
		 case -1:
			printf("\nThere is an error in the input. Exiting...........");
			exit(1);
//-------------------end of case -1 for isoperator----------------------------------------------------
		}
	}//end of for
	for_node=for_pop();
	return(for_node);
}//end of FTree_convert

//---------------------------------------------------------------------------------------------------
int add_predicate_to_formula_info(char pred[10],FInfo* formula_info)
{

	List_node* Lt=NULL;
		
	Lt=formula_info->PList->first;
	if(Lt==NULL){
//PList is empty
		List_node* new_node=new List_node;
		if(new_node==NULL){
			printf("\nMemory Allocation Error\n");
			exit(1);
		}
		strcpy(new_node->val,pred);
		new_node->right=new_node;
		new_node->left=new_node;

		formula_info->PList->first=new_node;
		formula_info->PList->last=new_node;

		formula_info->PList->size++;

		return 1;		
	}
	else{
//First check whether pred is already on the PList of formula_info
//if yes, return 0
//if no, add it to the front.
		do{

			if(strcmp(Lt->val,pred)==0)
				return 0;
			Lt=Lt->right;
		}while(Lt!=formula_info->PList->first);

//if we are here then it means pred is not in the PList
//generate a new list node and add it to the beginning of PList
	
		List_node* new_node=new List_node;
		if(new_node==NULL){
			printf("\nMemory Allocation Error\n");
			exit(1);
		}
		strcpy(new_node->val,pred);
		new_node->right=formula_info->PList->first;
		new_node->left=formula_info->PList->last;
		formula_info->PList->first->left=new_node;
		formula_info->PList->last->right=new_node;

		formula_info->PList->first=new_node;

		formula_info->PList->size++;

		return 1;
	}
}

//--------------------------------------------------------------------------------------------------
int add_variable_to_formula_info(char var[10],FInfo* formula_info)
{


	List_node* Lt=NULL;
	
	Lt=formula_info->VList->first;
	if(Lt==NULL){
//VList is empty
		List_node* new_node=new List_node;
		if(new_node==NULL){
			printf("\nMemory Allocation Error\n");
			exit(1);
		}
		strcpy(new_node->val,var);
		new_node->right=new_node;
		new_node->left=new_node;

		formula_info->VList->first=new_node;
		formula_info->VList->last=new_node;

		formula_info->VList->size++;

		return 1;		
	}
	else{
//First check whether pred is already on the VList of formula_info
//if yes, return 0
//if no, add it to the front.
		do{

			if(strcmp(Lt->val,var)==0)
				return 0;
			Lt=Lt->right;
		}while(Lt!=formula_info->VList->first);

//if we are here then it means pred is not in the VList
//generate a new list node and add it to the beginning of VList
	
		List_node* new_node=new List_node;
		if(new_node==NULL){
			printf("\nMemory Allocation Error\n");
			exit(1);
		}
		strcpy(new_node->val,var);
		new_node->right=formula_info->VList->first;
		new_node->left=formula_info->VList->last;
		formula_info->VList->first->left=new_node;
		formula_info->VList->last->right=new_node;

		formula_info->VList->first=new_node;

		formula_info->VList->size++;

		return 1;
	}
}


//---------------------------------------------------------------------------------------------------

void model_construct(FTree* TFormula,FInfo* formula_info)
{
/*
	int i;

	if(TFormula->type==0){//looking at variables
		printf("\nWe are looking at a variable:%s\n",TFormula->val);
		fflush(stdout);
		
		if(formula_info->VNum==0){
			printf("\nCurrently, there are no variables in the list\n");
			fflush(stdout);
			formula_info->VList[0]=malloc(5*sizeof(char));//5 is the maximum size of variable symbol we expect
			strcpy(formula_info->VList[0],TFormula->val);
			formula_info->VNum++;
			printf("\nAdded one variable to empty list");
			
			fflush(stdout);
			return;
		}
		printf("\nThere are one or more variables in the list\n");
		fflush(stdout);
		for(i=0;i<=(formula_info->VNum-1);i=i+1){
			if(strcmp(formula_info->VList[i],TFormula->val)==0)
				return;
		}
		formula_info->VList[i]=malloc(5*sizeof(char));
		strcpy(formula_info->VList[i],TFormula->val);
		formula_info->VNum++;
		return;
	}
	else if(TFormula->type==4){//looking at predicates
		printf("\nWe are looking at a predicate:%s\n",TFormula->val);
		fflush(stdout);

		if(formula_info->PNum==0){
			printf("\nCurrently, there are no predicates in the list\n");
			fflush(stdout);
			formula_info->PList[0]=malloc(5*sizeof(char));//5 is the maximum size of predicate symbol we expect.
			strcpy(formula_info->PList[0],TFormula->val);
			formula_info->PNum++;
			printf("\nAdded one predicate to empty list");
			fflush(stdout);
			//now call model_construct routine to add the variable attached with this predicate
			model_construct(TFormula->next,formula_info);
			return;
		}
		printf("\nThere are one or more predicates in the list\n");
		fflush(stdout);

		for(i=0;i<=(formula_info->PNum-1);i=i+1){
			if(strcmp(formula_info->PList[i],TFormula->val)==0)
				return;
		}
		formula_info->PList[i]=malloc(5*sizeof(char));
		strcpy(formula_info->PList[i],TFormula->val);
		formula_info->PNum++;
		//now call model_construct routine to add the variable attached with this predicate
		model_construct(TFormula->next,formula_info);
		return;
	}
	else if(TFormula->type==1){//subformula of type 'not'
		printf("\nWe are looking at an operator:%s",TFormula->val);
		model_construct(TFormula->next,formula_info);	
		return;	
	}
	else if(TFormula->type==2){//subformula of type 'and' 'or' 
		printf("\nWe are looking at an operator:%s",TFormula->val);
		model_construct(TFormula->left,formula_info);
		model_construct(TFormula->right,formula_info);
		return;
		
	}
	else if(TFormula->type==3){//subformula of type quantifier
		printf("\nWe are looking at a quantifier:%s",TFormula->val);
		model_construct(TFormula->next,formula_info);
		return;
		
	}
	else{
		printf("\nThere is an error in computing predicates and variables in the formula");
		exit;
	}
*/
}

//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------

FTree* eliminate_quantifier(FTree* TFormula,FInfo* formula_info)
{

	FTree* root=NULL;
	FTree* new_root=NULL;
	FTree* old_root=NULL;

	FTree* new_subformula=NULL;
	FTree* SubdTFnext=NULL;

	char var[10];
	int i,j,k;

	unsigned int M=formula_info->M;
	unsigned int R=formula_info->R;

//This is Okay. It works even when there are no MFO predicates in the input formula, only = (EQUALITY).

	printf("\nThe no. of models is %u\n", M);
	printf("\nThe no. of copies of models is %u\n",R);
//	getchar();

	printf("\nWe are in quantifier elimination routine.\n");
	printf("\nLooking at the following subformula:");
	if(TFormula==NULL)
		cout<<"\nScrewed\n";
	display_FTree_pre(TFormula);
	printf("\n");
/*
	printf("\nIn infix form:");
	//display_FTree(TFormula);
	printf("\n");
*/
	//we traverse the formula tree and replace Ex subtree by 2^M-1 'or' subtrees and replace Ax subtree by 2^M-1 'and' subtrees.
	switch(TFormula->type)
	{
		case 0:// type variable 
				return TFormula;

		case 1:// type unary connective
				cout<<"\nAt unary\n"<<TFormula->val;
				TFormula->next=eliminate_quantifier(TFormula->next,formula_info);
				return TFormula;

		case 2:// type binary connective
				cout<<"\nAt binary\n";
				TFormula->right=eliminate_quantifier(TFormula->right,formula_info);
				TFormula->left=eliminate_quantifier(TFormula->left,formula_info);
				return TFormula;

		case 3:// type quantifier
				cout<<"\nAt quant\n";		
				switch(TFormula->val[0])
				{
					case 'E': 	//replace TFormula tree with Ev at root by an 'or' rooted tree
							k=1;
							while(TFormula->val[k]!='\0'){
								var[k-1]=TFormula->val[k];
								k++;
							}
							var[k-1]='\0';
							printf("\nAfter eliminating quantifiers from the sub-formula:");
							display_FTree_pre_sub(TFormula->next);

							SubdTFnext=eliminate_quantifier(TFormula->next,formula_info);
							printf("\nWe get the following formula:");
							display_FTree_pre_sub(SubdTFnext);
							printf("\n\n");
							//getchar();

							printf("\nEliminating an existential quantifier E%s\n",var);
							////getchar();
//-----------------------------------no problem till here-----------------------------------------------------------------
							for(i=0;i<M;i++){
								for(j=0;j<R;j++){
									if(i==0 && j==0){
										root=new FTree;
										strcpy(root->val,"|");
										root->type=2;

										new_subformula=copy(SubdTFnext);

										printf("\nCopy of the subformula with free variable %s:",var);
										display_FTree_pre_sub(new_subformula);
										printf("\n");

										printf("\nReplacing %s by %d in the above formula",var,R*i+j);
										////getchar();

										substitute(new_subformula,var,0,0,R*0+0);

										printf("\nThe substituted subformula is:");
										display_FTree_pre_sub(new_subformula);
										printf("\n\n");

										root->left=new_subformula;
										root->right=NULL;

										printf("\nThe partially quantifier eliminated formula is:");
										display_FTree_pre_sub(root);
										printf("\n\n");

										old_root=root;
									}
									else if(i==M-1 && j==R-1){
										new_subformula=copy(SubdTFnext);

										printf("\nCopy of the subformula with free variable %s:",var);
										display_FTree_pre_sub(new_subformula);
										printf("\n");

										printf("\nReplacing %s by %d in the above formula",var,R*i+j);
										////getchar();

										substitute(new_subformula,var,i,j,R*i+j);

										printf("\nThe substituted subformula is:");
										display_FTree_pre_sub(new_subformula);
										printf("\n\n");

										old_root->right=new_subformula;

										printf("\nThe partially quantifier eliminated formula is:");
										display_FTree_pre_sub(old_root);
										printf("\n\n");

									}
									else{
										new_root=new FTree;
										strcpy(new_root->val,"|");
										new_root->type=2;

										new_subformula=copy(SubdTFnext);

										printf("\nCopy of the subformula with free variable %s:",var);
										display_FTree_pre_sub(new_subformula);
										printf("\n");

										printf("\nReplacing %s by %d in the above formula",var,R*i+j);
										////getchar();

										substitute(new_subformula,var,i,j,R*i+j);

										printf("\nThe substituted subformula is:");
										display_FTree_pre_sub(new_subformula);
										printf("\n");

										new_root->left=new_subformula;
										new_root->right=NULL;

										printf("\nThe partially quantifier eliminated formula is:");
										display_FTree_pre_sub(root);
										printf("\n");

										old_root->right=new_root;
										old_root=new_root;
									}
								}//end of for-j
							}//end of for-i
							return root;




					case 'A':	//replace TFormula tree with Av at root by an 'and' rooted tree
							
							k=1;
							while(TFormula->val[k]!='\0'){
								var[k-1]=TFormula->val[k];
								k++;
							}
							var[k-1]='\0';
							printf("\nEliminating an universal quantifier A%s\n",var);
							SubdTFnext=eliminate_quantifier(TFormula->next,formula_info);

							printf("\nAfter eliminating quantifiers from the sub-formula:");
							display_FTree_pre_sub(SubdTFnext);
							printf("\n\n");							
							//getchar();

							for(i=0;i<M;i++){
								for(j=0;j<R;j++){
									if(i==0 && j==0){
										root=new FTree;
										strcpy(root->val,"&");
										root->type=2;

										new_subformula=copy(SubdTFnext);

										printf("\nCopy of the subformula with free variable %s:",var);
										display_FTree_pre_sub(new_subformula);
										printf("\n");
										//getchar();

										substitute(new_subformula,var,0,0,R*0+0);

										printf("\nThe substituted subformula is:");
										display_FTree_pre_sub(new_subformula);
										printf("\n");

										root->left=new_subformula;

										printf("\nThe partially quantifier eliminated formula is:");
										display_FTree_pre_sub(root);
										printf("\n");

										old_root=root;
									}
									else if(i==M-1 && j==R-1){
										new_subformula=copy(SubdTFnext);

										printf("\nCopy of the subformula with free variable %s:",var);
										display_FTree_pre_sub(new_subformula);
										printf("\n");
										//getchar();

										substitute(new_subformula,var,i,j,R*i+j);

										printf("\nThe substituted subformula is:");
										display_FTree_pre_sub(new_subformula);
										printf("\n");

										old_root->right=new_subformula;

										printf("\nThe partially quantifier eliminated formula is:");
										display_FTree_pre_sub(old_root);
										printf("\n");

										
									}
									else{
										new_root=new FTree;
										strcpy(new_root->val,"&");
										new_root->type=2;

										new_subformula=copy(SubdTFnext);

										printf("\nCopy of the subformula with free variable %s:",var);
										display_FTree_pre_sub(new_subformula);
										printf("\n");

										//getchar();

										substitute(new_subformula,var,i,j,R*i+j);

										printf("\nThe substituted subformula is:");
										display_FTree_pre_sub(new_subformula);
										printf("\n");

										new_root->left=new_subformula;
										old_root->right=new_root;

										printf("\nThe partially quantifier eliminated formula is:");
										display_FTree_pre_sub(root);
										printf("\n");

										old_root=new_root;
									}
								}
							}
							return root;


					default: 	printf("\nThere is an error in the subformula.\nWe expected a quantifier.\n");
							exit;
				}			

		case 4:// type predicate
				cout<<"\nAt predicate\n";
				return TFormula;

	}
}
//--------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------
FTree* copy(FTree* TFormula)
{

	 FTree* new_leaf=NULL;
	 FTree* new_node=NULL;
	 FTree* new_subtree=NULL;
	 FTree* new_l_subtree=NULL;
	 FTree* new_r_subtree=NULL;

	//printf("\nIn the copy routine");
	if(TFormula==NULL){
		//printf("\nError in the formula tree.\n");
		return(NULL);
	}
			printf("\nThe formula to copy (in prefix form)");
			display_FTree_pre_sub(TFormula);	
			fflush(stdout);

	switch(TFormula->type){
		case 2: //printf("\nThe node type is %d",TFormula->type);
			new_l_subtree=copy(TFormula->left);
			new_r_subtree=copy(TFormula->right);

			new_node= new FTree;
			if(new_node==NULL){
				//printf("\nError in memory allocation\nExiting\n");
				exit(0);
			}
			new_node->type=2;
			new_node->subd=0;
			new_node->model=-1;
			new_node->copy=-1;
			new_node->num=-1;

//			new_node->global=0;
			strcpy(new_node->val,TFormula->val);
			new_node->left=new_l_subtree;
			new_node->right=new_r_subtree;
			new_node->next=NULL;
			return(new_node);
		case 3:
		case 4:
		case 1: //printf("\nThe node type is %d",TFormula->type);
			fflush(stdout);
			new_subtree=copy(TFormula->next);
			new_node=new FTree;
			if(new_node==NULL){
				//printf("\nError in memory allocation\nExiting\n");
				exit(0);
			}
			new_node->type=TFormula->type;
//			new_node->global=0;
			new_node->subd=0;
			new_node->model=-1;
			new_node->copy=-1;
			new_node->num=-1;

			strcpy(new_node->val,TFormula->val);
			new_node->left=NULL;
			new_node->right=NULL;
			new_node->next=new_subtree;
			return(new_node);

		case 0: //printf("\nThe node type is %d",TFormula->type);
			fflush(stdout);
			new_leaf=new FTree;
			if(new_leaf==NULL){
				//printf("\nError in memory allocation\nExiting\n");
				exit(0);
			}
			new_leaf->type=0;
//			new_leaf->global=0;
			new_leaf->subd=TFormula->subd;
			new_leaf->model=TFormula->model;	
			new_leaf->copy=TFormula->copy;
			new_leaf->num=TFormula->num;

			strcpy(new_leaf->val,TFormula->val);
			cout<<"\nNew leaf val is "<<new_leaf->val;
			cout<<"\nTF val is "<<TFormula->val;
			new_leaf->left=NULL;
			new_leaf->right=NULL;
			new_leaf->next=NULL;

			return(new_leaf);		
			

		default:
			//printf("\nError! Type can either be 0, 1 or 2.\n");
			break;

	}
}
//--------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------------------------------------------
void substitute(FTree* TFormula,char var[10], int i, int j,int k)
{
//	printf("\nReplacing %s by (%d,%d) recursively in the formula:",var,i,j);
//	//display_FTree_pre(TFormula);
//	printf("\n");
	if(TFormula==NULL){
		printf("\nFormula is NULL\nThis should not have happened.\n");
		fflush(stdout);
		return;
	}
	switch(TFormula->type)
	{
		case 0:		if(strcmp(TFormula->val,var)==0 && TFormula->subd ==0){
					printf("We are replacing %s by (%d,%d) at the leaf level\n",TFormula->val,i,j);
					TFormula->model=i;
					TFormula->copy=j;
					TFormula->num=k;
					TFormula->subd=1;
				}
				return;

		case 2:		substitute(TFormula->left,var,i,j,k);
				substitute(TFormula->right,var,i,j,k);
				return;	

		case 1:
		case 3:
		case 4:
				substitute(TFormula->next,var,i,j,k);
				return;
	}
}
//--------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------------------------------------------------





//------------------------------------------------------------------------------------------------------------------------------------------
int strchk(char* str)
{
 switch (str[0])
 {
//-------------------------UNARY------------------------------------- 
 case '~'://NOT

 case 'X'://NEXT
 case 'Y'://ONE-STEP PAST

 case 'F'://DIAMOND
 case 'G'://BOX

 case 'P'://DIAMOND MINUS
 case 'Q'://BOX MINUS


	return 1;
//-------------------------BINARY-------------------------------------
 case '|'://OR
 case '&'://AND
 case '='://EQUALS

 case 'U'://UNTIL
 case 'V'://RELEASE
	return 2;
//------------------------QUANTIFIERS---------------------------------
 case 'E':
 case 'A':
	return 3;

//------------------------MONADIC PREDICATES----------------------------------
 case 'p':
 case 'q':
 case 'r':
 case 's':
 case 't':
	return 4;
 }
}
//-------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------------------------------------------------
int prcd(char symbol)
{
 switch (symbol)
 {
//-------------------------UNARY------------------------------------- 
 case '~'://NOT

 case 'X'://NEXT
 case 'Y'://ONE-STEP PAST

 case 'F'://DIAMOND
 case 'G'://BOX

 case 'P'://DIAMOND MINUS
 case 'Q'://BOX MINUS

 case 'E':
 case 'A':

 case 'p':
 case 'q':
 case 'r':
 case 's':
 case 't':

//-------------------------BINARY-------------------------------------
 case '|'://OR
 case '&'://AND
 case '='://EQUALS

 case 'U'://UNTIL
 case 'V'://RELEASE
	return 2;

 case '(':
	return 0;
 case ')':
 	return 1;
 }
 
}
//--------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------------
int isoperator(char symbol)
{
 switch (symbol)
 {
 	case '~'://~,V,&,X,U,R,P,Q,Y,F,G
 	case '|':
 	case '&':
 	case '=':
 	
 	case 'X':
 	case 'Y':
 	case 'U':


 	case 'V':
 	case 'F':
 	case 'G':
 	case 'P':
 	case 'Q':

 	case 'E':
 	case 'A':
	
 	case 'p':
 	case 'q':
 	case 'r':
 	case 's':
 	case 't':

 	case '(':
 	case ')':
 		return 1;//non-variables
 	case 'x':
 	case 'y':
 	case 'z':
 		return 0;//variables

	default: 
		return -1;//illegal input
 }
 
}
//--------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------------

void for_push(FTree* item)
{
 for_top++;
 for_stack[for_top] = item;
 
}
//--------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------------

void op_push(FTree* item)
{
 op_top++;
 op_stack[op_top] = item;
 
}
//--------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------------
 
FTree* for_pop()
{
 FTree* a=NULL;
 a = for_stack[for_top];
 for_top--;
 return(a);
 
} 
//--------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------------

FTree* op_pop()
{
 FTree* a=NULL;
 a = op_stack[op_top];
 op_top--;
 return(a);
 
}
//--------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------
